package pentastagiu.webservice;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.model.Images;
import pentastagiu.service.ImagesService;

@Path("/image")
public class ImagesWebService {
	@Inject
	private ImagesService imagesService;

	/**
	 * Get Images from the DB
	 * 
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Images> findAll() {
		return imagesService.findAll();
	}

	/**
	 * Get Images by id
	 * 
	 */
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Images findById(@PathParam("id") Long id) {
		return imagesService.findById(id);
	}

	/**
	 * Insert Images
	 * 
	 * @param emp
	 * @return
	 */
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Images insert(Images Images) {
		return imagesService.insertImages(Images);
	}

	/**
	 * Update Images
	 * 
	 * @param emp
	 * @return
	 */
	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Images update(Images Images) {
		return imagesService.updateImages(Images);
	}

	/**
	 * Delete images
	 * 
	 * @param id
	 */
	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public void remove(@PathParam("id") Long id) {
		imagesService.deleteImages(id);
	}
}
