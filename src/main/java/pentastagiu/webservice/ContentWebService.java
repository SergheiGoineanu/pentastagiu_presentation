package pentastagiu.webservice;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.model.Content;
import pentastagiu.service.ContentService;

@Path("/content")
public class ContentWebService {
	@Inject
	private ContentService contentService;

	/**
	 * Get content from the DB
	 * 
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Content> findAll() {
		return contentService.findAll();
	}

	/**
	 * Get content by id
	 * 
	 */
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Content findById(@PathParam("id") Long id) {
		return contentService.findById(id);
	}

	/**
	 * Insert content
	 * 
	 * @param emp
	 * @return
	 */
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Content insert(Content content) {
		return contentService.insertContent(content);
	}

	/**
	 * Update content
	 * 
	 * @param emp
	 * @return
	 */
	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Content update(Content content) {
		return contentService.updateContent(content);
	}

	/**
	 * Delete content
	 * 
	 * @param id
	 */
	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public void remove(@PathParam("id") Long id) {
		contentService.deleteContent(id);
	}
}
