package pentastagiu.binder;

import org.glassfish.jersey.server.ResourceConfig;

public class ContentWebServiceConfig extends ResourceConfig {
	public ContentWebServiceConfig() {
		register(new ContentWebServiceBinder());
		packages(true, "pentastagiu.webservice");
	}
}
