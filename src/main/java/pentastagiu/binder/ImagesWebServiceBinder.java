package pentastagiu.binder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import pentastagiu.dao.ImagesDAO;
import pentastagiu.service.ImagesService;
import pentastagiu.service.implementation.ImagesServiceImpl;

public class ImagesWebServiceBinder extends AbstractBinder {
	@Override
	protected void configure() {
		bind(ImagesServiceImpl.class).to(ImagesService.class);
		bind(ImagesDAO.class).to(ImagesDAO.class);
	}
}
