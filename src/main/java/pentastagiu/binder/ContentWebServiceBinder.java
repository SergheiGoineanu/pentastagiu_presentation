package pentastagiu.binder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import pentastagiu.dao.ContentDAO;
import pentastagiu.service.ContentService;
import pentastagiu.service.implementation.ContentServiceImpl;

public class ContentWebServiceBinder extends AbstractBinder {
	@Override
	protected void configure() {
		bind(ContentServiceImpl.class).to(ContentService.class);
		bind(ContentDAO.class).to(ContentDAO.class);
	}
}
