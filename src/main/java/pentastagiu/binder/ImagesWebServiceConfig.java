package pentastagiu.binder;

import org.glassfish.jersey.server.ResourceConfig;

public class ImagesWebServiceConfig extends ResourceConfig {
	public ImagesWebServiceConfig() {
		register(new ImagesWebServiceBinder());
		packages(true, "pentastagiu.webservice");
	}
}
