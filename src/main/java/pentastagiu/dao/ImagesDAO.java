package pentastagiu.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pentastagiu.model.Images;
import pentastagiu.service.ImagesService;

@Resource
@ManagedBean
public class ImagesDAO implements ImagesService {

	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("Images");
	private EntityManager entityManager = emfactory.createEntityManager();

	/**
	 * List Images
	 * 
	 */
	public List<Images> findAll() {
		Query query = entityManager.createQuery("SELECT image FROM Images image");
		List<Images> ImagesList = new ArrayList<Images>();
		try {
			ImagesList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ImagesList;
	}

	/**
	 * Find Images by id
	 */
	public Images findById(Long id) {
		Images Images = null;
		try {
			Images = entityManager.find(Images.class, id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Images;
	}

	/**
	 * Insert new Images
	 */
	public Images insertImages(Images Images) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(Images);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Images;
	}

	/**
	 * Update Images
	 */
	public Images updateImages(Images Images) {

		try {
			entityManager.getTransaction().begin();
			entityManager.merge(Images);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Images;
	}

	/**
	 * Delete Images
	 */
	public Long deleteImages(Long id) {
		Images Images = null;
		try {
			entityManager.getTransaction().begin();
			Images = entityManager.find(Images.class, id);
			entityManager.remove(Images);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
}
