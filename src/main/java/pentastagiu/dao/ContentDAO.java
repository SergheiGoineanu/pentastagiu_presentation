package pentastagiu.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pentastagiu.model.Content;
import pentastagiu.service.ContentService;

@Resource
@ManagedBean
public class ContentDAO implements ContentService {

	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("Content");
	private EntityManager entityManager = emfactory.createEntityManager();

	/**
	 * List content
	 * 
	 */
	public List<Content> findAll() {
		Query query = entityManager.createQuery("SELECT c FROM Content c");
		List<Content> contentList = new ArrayList<Content>();
		try {
			contentList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contentList;
	}

	/**
	 * Find content by id
	 */

	public Content findById(Long id) {
		Content content = null;
		try {
			content = entityManager.find(Content.class, id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * Insert new content
	 */
	public Content insertContent(Content content) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(content);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * Update content
	 */
	public Content updateContent(Content content) {

		try {
			entityManager.getTransaction().begin();
			entityManager.merge(content);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * Delete content
	 */
	public Long deleteContent(Long id) {
		Content content = null;
		try {
			entityManager.getTransaction().begin();
			content = entityManager.find(Content.class, id);
			entityManager.remove(content);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
}
