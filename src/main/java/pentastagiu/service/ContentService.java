package pentastagiu.service;

import java.util.List;

import pentastagiu.model.Content;

public interface ContentService {

	/**
	 * List all content
	 * 
	 */
	List<Content> findAll();

	/**
	 * Find content by id
	 * 
	 * @param id
	 * @return
	 */
	Content findById(Long id);

	/**
	 * Insert new content
	 * 
	 * @param content
	 * @return
	 */
	Content insertContent(Content content);

	/**
	 * Update content
	 * 
	 * @param content
	 * @return
	 */
	Content updateContent(Content content);

	/**
	 * Delete content
	 * 
	 * @param id
	 * @return
	 */
	Long deleteContent(Long id);
}
