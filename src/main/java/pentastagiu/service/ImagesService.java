package pentastagiu.service;

import java.util.List;

import pentastagiu.model.Images;

public interface ImagesService {
	/**
	 * List all Images
	 * 
	 */
	List<Images> findAll();

	/**
	 * Find Images by id
	 * 
	 * @param id
	 * @return
	 */
	Images findById(Long id);

	/**
	 * Insert new Images
	 * 
	 * @param Images
	 * @return
	 */
	Images insertImages(Images Images);

	/**
	 * Update Images
	 * 
	 * @param Images
	 * @return
	 */
	Images updateImages(Images Images);

	/**
	 * Delete Images
	 * 
	 * @param id
	 * @return
	 */
	Long deleteImages(Long id);
}
