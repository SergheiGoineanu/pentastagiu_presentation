package pentastagiu.service.implementation;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.dao.ContentDAO;
import pentastagiu.model.Content;
import pentastagiu.service.ContentService;

public class ContentServiceImpl implements ContentService {
	@Inject
	private ContentDAO contentDAO;

	/**
	 * List content
	 * 
	 */
	public List<Content> findAll() {
		return contentDAO.findAll();
	}

	/**
	 * Find content by id
	 * 
	 */
	public Content findById(Long id) {
		return contentDAO.findById(id);
	}

	/**
	 * Insert new content
	 * 
	 */
	public Content insertContent(Content content) {
		return contentDAO.insertContent(content);
	}

	/**
	 * Update Content
	 * 
	 */
	public Content updateContent(Content content) {
		return contentDAO.updateContent(content);
	}

	/**
	 * Delete Content
	 * 
	 */
	public Long deleteContent(Long id) {
		return contentDAO.deleteContent(id);
	}
}
