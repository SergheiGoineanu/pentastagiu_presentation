package pentastagiu.service.implementation;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.dao.ImagesDAO;
import pentastagiu.model.Images;
import pentastagiu.service.ImagesService;

public class ImagesServiceImpl implements ImagesService {
	@Inject
	private ImagesDAO ImagesDAO;

	/**
	 * List Images
	 * 
	 */
	public List<Images> findAll() {
		return ImagesDAO.findAll();
	}

	/**
	 * Find Images by id
	 * 
	 */
	public Images findById(Long id) {
		return ImagesDAO.findById(id);
	}

	/**
	 * Insert new Images
	 * 
	 */
	public Images insertImages(Images Images) {
		return ImagesDAO.insertImages(Images);
	}

	/**
	 * Update Images
	 * 
	 */
	public Images updateImages(Images Images) {
		return ImagesDAO.updateImages(Images);
	}

	/**
	 * Delete Images
	 * 
	 */
	public Long deleteImages(Long id) {
		return ImagesDAO.deleteImages(id);
	}
}
