package pentastagiu.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
	@Entity
	public class Images implements Serializable {
		private static final long serialVersionUID = 1L;
		@Id
		private Long id;
		@Column
		private String name;
		@Column(columnDefinition = "text")
		private String link;
		
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
