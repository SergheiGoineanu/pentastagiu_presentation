var app = angular.module('PentaStagiuApp', [ 'ngRoute' ]);

app.config([ "$routeProvider", "$locationProvider",
		function($routeProvider, $locationProvider) {
			$routeProvider.when("/", {
				templateUrl : "index.html",
				controller : "ContentController"
			})
		} ]);

app.controller('ContentController', function($scope, $http) {

	$scope.showContent = true;

	$scope.ctnt = null;

	$scope.getContent = function() {
		$http({
			method : 'GET',
			url : '/presentation/restc/',
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(result) {
			$scope.content = result;
			$scope.showContent = true;
		});
	};
});